## Release Summary

Changed since last submission:

* In DESCRIPTION changed "stats::setNames" -> "stats::setNames()"
* Removed not-yet-existing URL http://www.r-pkg.org/pkg/setter

## Test Environments

* Local Windows 7 & 10, R-devel 
* Semaphore CI + Ubuntu 14.04, R-devel and R-release
* AppVeyor + Windows Server 2012, R-devel

## R CMD check results

There were no ERRORs or WARNINGs.

## Downstream dependencies

Nothing downstream yet.
